<?php declare(strict_types=1);

namespace Terah\Wkhtml;

use Exception;
use Terah\Assert\Assert;

use Psr\Log\LoggerAwareTrait;

class Wkhtml
{
    use LoggerAwareTrait;

    protected string $tempPath      = '';

    protected string $tempFile      = '';

    protected bool $doCacheFile     = true;

    protected string $wkhtmlBinary  = '';

    protected string $wkhtmlArgs    = '';

    protected string $orientation   = 'portrait';

    protected string $uriSource     = '';

    protected string $stringSource  = '';

    protected string $content       = '';

    protected bool $generated       = false;

    protected array $xvfbLocations  = [
        '/usr/bin/xvfb-run', // Ubuntu
    ];

    protected ?string $xvfbBinary   = '/usr/bin/xvfb-run';

    protected ?string $xvfbArgs     = '--server-args="-screen 0, 1024x768x24"';

    protected array $wkhtmlLocations = [
        '/usr/bin/wkhtmltopdf', // Ubuntu
        '/usr/local/bin/wkhtmltopdf', // Mac
    ];


    public function __construct(array $settings)
    {
        $wkhtmltopdf            = $xvfb = $xvfb_args = '';
        extract($settings);
        $wkhtmltopdf            = $this->findWkhtml($wkhtmltopdf);
        Assert::that($wkhtmltopdf)->notEmpty('Could not find the wkhtmltopdf binary installed on your system.')->file('Could not find the wkhtmltopdf binary installed on your system.');
        $this->wkhtmlBinary     = $wkhtmltopdf;
        if ( $xvfb === false )
        {
            $this->xvfbBinary       = $this->xvfbArgs = '';
        }
        if ( $xvfb )
        {
            $this->xvfbBinary($this->findXvfb($xvfb));
            $this->xvfbArgs         = $xvfb_args;
        }
    }


    protected function findWkhtml(string $wkhtmlBinary) : string
    {
        if ( $wkhtmlBinary && file_exists($wkhtmlBinary) )
        {
            return $wkhtmlBinary;
        }
        foreach ( $this->wkhtmlLocations as $path )
        {
            if ( file_exists($path) )
            {
                return $path;
            }
        }

        return '';
    }


    protected function findXvfb(string $xvfbBinary='') : string
    {
        if ( $xvfbBinary && file_exists($xvfbBinary) )
        {
            return $xvfbBinary;
        }
        foreach ( $this->xvfbLocations as $path )
        {
            if ( file_exists($path) )
            {
                return $path;
            }
        }

        return '';
    }


    public function wkhtmlBinary(string $wkhtmlBinary) : Wkhtml
    {
        Assert::that($wkhtmlBinary)->file("The wkhtml binary does not exist.");
        $this->wkhtmlBinary     = $wkhtmlBinary;

        return $this;
    }


    public function wkhtmlArgs(string $wkhtmlArgs) : Wkhtml
    {
        $this->wkhtmlArgs   = $wkhtmlArgs;

        return $this;
    }


    public function xvfbBinary(string $xvfbBinary) : Wkhtml
    {
        Assert::that($xvfbBinary)->file("The xvfb binary does not exist.");
        $this->xvfbBinary       = $xvfbBinary;

        return $this;
    }


    public function xvfbArgs(string $xvfbArgs) : Wkhtml
    {
        $this->xvfbArgs         = $xvfbArgs;

        return $this;
    }


    public function fromUri(string $source) : Wkhtml
    {
        $this->uriSource        = $source;
        $this->stringSource     = '';
        $this->generated        = false;

        return $this;
    }


    public function fromFile(string $source) : Wkhtml
    {
        $this->uriSource        = '';
        $this->stringSource     = file_get_contents($source);
        $this->generated        = false;

        return $this;
    }


    public function fromString(string $source) : Wkhtml
    {
        $this->uriSource        = '';
        $this->stringSource     = $source;
        $this->generated        = false;

        return $this;
    }


    public function orientation(string $orientation) : Wkhtml
    {
        $orientation            = ucfirst(strtolower($orientation));
        Assert::that($orientation)->inArray(['Landscape', 'Portrait'], "The orientation must be either landscape or portrait.");
        $this->orientation      = $orientation;
        $this->generated        = false;

        return $this;
    }


    public function generate() : Wkhtml
    {
        if ( $this->generated )
        {
            return $this;
        }
        Assert::that($this->stringSource)->notEmpty('There is no content to generate a pdf from');
        $descriptorSpec         = [
            0                       => ['pipe', 'r'], // stdin
            1                       => ['pipe', 'w'], // stdout
            2                       => ['pipe', 'w'], // stderr
        ];

        $orientation            = '--orientation ' . escapeshellarg($this->orientation);
        $wkhtmlBinary           = escapeshellarg($this->wkhtmlBinary);
        $command                = "{$wkhtmlBinary} -q {$orientation} {$this->wkhtmlArgs} - -";
        if ( $this->xvfbBinary )
        {
            $command                = str_replace('wkhtmltopdf', 'wkhtmltopdf --use-xserver --page-size A4', $command);
            $command                = "{$this->xvfbBinary} {$this->xvfbArgs} {$command}";
        }
        $this->logger->info("Generating PDF with command: ({$command})");
        $process                = proc_open($command, $descriptorSpec, $pipes);
        fwrite($pipes[0], $this->stringSource);
        fclose($pipes[0]);
        // Read the outputs
        $this->content          = stream_get_contents($pipes[1]);
        $errors                 = stream_get_contents($pipes[2]);
        // Close the process
        fclose($pipes[1]);
        proc_close($process);

        if ( $errors )
        {
            $this->error('Failed to generate PDF. Command: ' . $command, [$errors]);
        }
        $this->generated        = true;

        return $this;
    }


    public function toBrowser(string $fileName='pdf_file') : void
    {
        $this->generate();
        header("Content-Disposition:attachment;filename='{$fileName}'");
        header('Content-type: application/pdf');
        echo $this->content;

        exit;
    }


    public function toFile(string $fullPath) : bool
    {
        $this->generate();
        Assert::that($this->content)->notEmpty("The generated pdf file is empty");
        if ( ! file_put_contents($fullPath, $this->content) )
        {
            $this->error("Failed to copy PDF to {$fullPath}");
        }

        return true;
    }


    public function toString() : string
    {
        $this->generate();
        Assert::that($this->content)->notEmpty("The generated pdf file is empty");

        return $this->content;
    }


    protected function error(string $message, array $context=[]) : void
    {
        $this->logger->error($message, $context);
        $context                = $context ? ": " . print_r($context, true) : '';

        throw new Exception($message . $context);
    }
}
