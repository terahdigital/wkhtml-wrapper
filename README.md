# wkhtml-wrapper

[![Latest Version](https://img.shields.io/github/release/terah/wkhtml-wrapper.svg?style=flat-square)](https://bitbucket.org/terahdigital/wkhtml-wrapper/releases)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Build Status](https://img.shields.io/travis/terah/wkhtml-wrapper/master.svg?style=flat-square)](https://travis-ci.org/terah/wkhtml-wrapper)
[![Coverage Status](https://img.shields.io/scrutinizer/coverage/g/terah/wkhtml-wrapper.svg?style=flat-square)](https://scrutinizer-ci.com/g/terah/wkhtml-wrapper/code-structure)
[![Quality Score](https://img.shields.io/scrutinizer/g/terah/wkhtml-wrapper.svg?style=flat-square)](https://scrutinizer-ci.com/g/terah/wkhtml-wrapper)
[![Total Downloads](https://img.shields.io/packagist/dt/terah/wkhtml-wrapper.svg?style=flat-square)](https://packagist.org/packages/terah/wkhtml-wrapper)

A simple class to provide a fluent interface to the wkhtmltopdf utility

## Install
Get the wkhtmltopdf binary from [http://wkhtmltopdf.org/](http://wkhtmltopdf.org/)

Get this library via Composer

``` bash
$ composer require terah/wkhtml-wrapper
```

## Usage

``` php

$pdf = new Terah\Wkhtml\Wkhtml('/path/to/wkhtmltopdf', '/path/to/xvfb-run', '--server-args="-screen 0, 1024x768x24"', new Psr\Log\NullLogger());


```
###Super basic usage

This will fetch http://google.com, generate a pdf and save it to your system tmp directory.

```php

$pdf->fromUri('http://google.com/')->generate();


```

###Input Options - URI, file path or html string

Generate pdfs from uri, file or html string and save to your tmp directory.

```php

$pdf->fromUri('http://google.com/')->generate();

$pdf->fromFile('/path/to/html/report.html')->generate();

$pdf->fromString('<html>...</html>')->generate();


```

###Output Options - File, Browser or pdf string

Create a pdf of http://google.com and save it to a file location.

```php
$pdf->fromUri('http://google.com/')->toFile('/path/to/save/file/to.pdf');
```
Create a pdf from /path/to/html/report.html and output to the browser with a name of my-fancy-report.pdf

```php
$pdf->fromFile('/path/to/html/report.html')->toBrowser('my-fancy-report.pdf');
```
Generate a pdf from http://google.com/ and return it to the $myPdfString variable

```php
$myPdfString = $pdf->fromUri('http://google.com/')->toString();
```

###Other options - Orientation and temp file locations


```php

$pdf->fromUri('http://google.com/')

	// With landscape orientation;
	->orienation('Landscape')
	// Set the path to xvfb-run
	->xvfbBinary('/path/to/xvfb-run')
	// Set the args for xvfb-run
	->xvfbArgs('--server-args="-screen 0, 1024x768x24"')
	// Ship it to the browser
	->toBrowser('my-report-name.pdf');


```

## Testing

``` bash
$ bin/tester tests/

```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email terry@terah.com.au instead of using the issue tracker.

## Credits

- [Terry Cullen](https://bitbucket.org/terahdigital)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
