<?php declare(strict_types=1);

namespace Terah\Assert\Test;

use Terah\Assert\Assert;
use Terah\Assert\Tester;
use Terah\Assert\Suite;
use Terah\Wkhtml\Wkhtml;

Tester::suite('WkhtmlSuite')

    ->fixture('wkhtml', (new Wkhtml([
        'wkhtmltopdf'       => '/usr/bin/wkhtmltopdf',
        'pdf.xvfb'          => '/usr/bin/xvfb-run',
         'xvfb_args'        => '-e /dev/stdout --server-args="-screen 0, 1024x768x24"',
        ]))

        ->tempPath('/tmp/'))

    ->test('testGeneratePdfFromUrl', function(Suite $suite) {

        if ( file_exists('/tmp/testing-pdf-logger.pdf') )
        {
            @unlink('/tmp/testing-pdf-logger.pdf');
            @unlink('/tmp/testing-pdf-logger-out.pdf');
        }
        $success = $suite->getFixture('wkhtml')
            ->fromUri('http://google.com/')
            ->tempFile('testing-pdf-logger.pdf')
            ->orientation('landscape')
            ->toFile('/tmp/testing-pdf-logger-out.pdf');

        Assert::that($success)->true();
    })

    ->test('testGeneratePdfFromString', function(Suite $suite) {

        if ( file_exists('/tmp/testing-pdf-logger.pdf') )
        {
            @unlink('/tmp/testing-pdf-logger.pdf');
            @unlink('/tmp/testing-pdf-logger-out.pdf');
        }
        $myPdfFile = $this->wkhtml
            ->fromString('<html><body><h1>Hello world</h1></body></html>')
            ->tempPath('/tmp/')
            ->tempFile('testing-pdf-logger.pdf')
            ->orientation('landscape')
            ->toString();

        Assert::that($myPdfFile)->notEmpty();
    })
;
